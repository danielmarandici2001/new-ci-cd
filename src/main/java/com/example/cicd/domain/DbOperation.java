package com.example.cicd.domain;

public enum DbOperation {
    SAVE, DELETE
}